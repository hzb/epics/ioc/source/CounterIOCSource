#!../../bin/linux-x86_64/Counter

< envPaths
epicsEnvSet("ENGINEER","WillSmith")

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/Counter.dbd"
Counter_registerRecordDeviceDriver pdbbase

## Load record instances
dbLoadRecords("db/Counter.db","SYS=$(IOC_SYS),DEV =$(IOC_DEV)")
#var streamDebug 1

cd "${TOP}/iocBoot/${IOC}"
iocInit

dbl > /opt/epics/ioc/log/logs.dbl
